import fire from './fire-config';
import {Item} from './schema'
import {Document, QuerySnapshot, mapToDocs} from './model'

export const getAllItems = (setItem:(items: Document<Item>[])=>void) => {
    let items = fire.firestore()
    .collection('items')
    .onSnapshot(snap => {
        const items  = mapToDocs<Item>(snap as QuerySnapshot<Item>)
        setItem(items)
    });
    return(items)
}

export const getFavItems = (setFavItem:(items: Document<Item>[])=>void) => {
    let items =  fire.firestore()
    .collection('items')
    .where("favourite", "==", true)
    .onSnapshot(snap => {
        const items  = mapToDocs<Item>(snap as QuerySnapshot<Item>)
        setFavItem(items)
    });
    return(items)
}

export const getFilteredItems = (filter:string, setItem:(items: Document<Item>[])=>void) => {
    let items =  fire.firestore()
    .collection('items')
    .where('hashtags', 'array-contains', filter )
    .onSnapshot(snap => {
        const items  = mapToDocs<Item>(snap as QuerySnapshot<Item>)
        setItem(items)
    });
    return(items)
}

export const updateDb = (itemId:string, update: Item) => {
    fire.firestore()
    .collection('items')
    .doc(itemId)
    .update(update);
}
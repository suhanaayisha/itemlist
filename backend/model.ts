import firebase from 'firebase';

export interface Document<T> {
    id: string
    doc: T
  }

export interface QueryDocumentSnapshot<T> extends firebase.firestore.QueryDocumentSnapshot {
    data(): T;
}
export interface QuerySnapshot<T> extends firebase.firestore.QuerySnapshot {
    docs: QueryDocumentSnapshot<T>[];
    forEach(callback: (result: QueryDocumentSnapshot<T>) => void, thisArg?: any): void;
}

export function mapToDocs<T>(docs: QuerySnapshot<T>) {
    return docs.docs.map((doc) => {
        const v: Document<T> = {
            id: doc.id,
            doc: doc.data(),
        };
        return v;
    });
}
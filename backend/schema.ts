//since it is a single user system, favourite is a boolean value in the list of items

export interface Item {
    asset_image: string;
    comments: string;
    favourite: boolean;
    hashtags: string[];
    likes: string;
    owner: string;
    title: string;
    user_image: string;
    price: string
}  
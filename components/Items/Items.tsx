import { Row, Col, Card, Avatar } from 'antd';
import {
    HeartFilled,
    HeartOutlined
} from '@ant-design/icons';
import { NextPage } from 'next';
import {Item} from '../../backend/schema';
import { updateDb } from '../../backend/queries'
import {Document} from '../../backend/model';
import fire from '../../backend/fire-config';

interface Props{
    items:  Document<Item>[]
}

const Items: NextPage<Props> = (props) => {

    return(
        <Row gutter={[{ lg: 20, sm: 0 },{ lg: 20, sm: 0 }]} className="items">
        {props.items.map(item => {
          return(
            <Col xs={24} md={12} lg={12} xl={8}>
              <Card>
                <div className="user-details">
                  <Avatar size="large" src={item.doc.user_image} />
                  <span className="name">{item.doc.owner}</span>
                </div>
                <div className="image-container">
                  <img src={item.doc.asset_image}/>
                  <div className="image-details">
                    <div>
                      <p>{item.doc.title}</p>
                      <p className="price">AED {item.doc.price}</p>
                    </div>
                    <div className="heart">
                      {item.doc.favourite ? 
                      <HeartFilled onClick={() => updateDb(item.id, {...item.doc, favourite: false})}/>
                      :
                      <HeartOutlined onClick={() => updateDb(item.id, {...item.doc, favourite: true})}/>
                      }
                    </div>
                  </div>
                </div>
                <div className="last-section">
                  <div className="likes">
                    <HeartFilled/> 
                        <span className="no-of-likes">
                          {item.doc.likes} likes
                        </span>
                    
                  </div>
                  <p className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                  <p className="hashtags">#{item.doc.hashtags.join(' #')}...</p>
                  <p className="comments">View {item.doc.comments} comments </p>
                </div>
              </Card>
            
          </Col>
          )
        }
        )}
        <div style={{height:'64px', color:'#fff'}} className='mobile-view'>
          Scroll
        </div>
        
      </Row>
    )

}

export default Items
import { isMobile } from "react-device-detect";
import Link from 'next/link'
import { Layout, Menu } from "antd";
import { useRouter } from 'next/router'
import {
  HomeFilled,
  HeartFilled
} from '@ant-design/icons';

const { Header } = Layout;

const HeaderBar = () => {
  const router = useRouter()

  return (
    <Header className={"app-header"}>
      <div className="container">
        <div className="title hide-mobile">
          <h1>ItemList</h1>
        </div>
        <Menu mode="horizontal" onClick={(e)=>console.log(e.key)} defaultSelectedKeys={router.pathname.includes("liked")? ["/liked"]: ["/home"]}>
          <Menu.Item
            key="/home"
          >
            <Link href="/home">
              {isMobile?
              <HomeFilled />
              :
              "Home"
              }
            </Link>
          </Menu.Item>
          <Menu.Item
            key="/liked"
          >
            <Link href="/liked">
            {isMobile?
              <HeartFilled />
              :
              "Liked"
              }
            </Link>
          </Menu.Item>
        </Menu>
      </div>
    </Header>
  );
};

export default HeaderBar;

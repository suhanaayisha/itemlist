import { useState } from 'react';
import { Row } from 'antd';
import {
    ArrowLeftOutlined
} from '@ant-design/icons';
import { NextPage } from 'next';
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";

interface Props{
    tags: string[]
    filter: (hashtag:string) => void
    goBack: () => void
}
const HashtagFilter:NextPage<Props> = (props) => {

    const [showHashtags, setShowHashTags] = useState<boolean>(true)
    const shuffle = (a:string[]) => {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;

    }

      
    if(showHashtags){
        return(
            <Row
            className="hashtags"
            justify="space-between"
            align="middle"
          >
            <Swiper
              style={{
                direction: "ltr"
              }}
              spaceBetween={20}
              slidesPerView={"auto"}
              speed={1000}
            >
              {shuffle(props.tags).map((item) => (
                <SwiperSlide>
                       <div 
                        className="tag" 
                        onClick={()=> {
                            setShowHashTags(false)
                            props.filter(item)}
                        }>
                           {item}
                       </div>
                </SwiperSlide>
              ))}
            </Swiper>
          </Row>
            
        )

    }
    else{
        return(
            <div 
                className="back-btn"
                onClick={()=>{
                    setShowHashTags(true)
                    props.goBack()
                }}>
                <ArrowLeftOutlined style={{marginRight:'5px'}}/> Back
            </div>
        )
    }
    

}

export default HashtagFilter

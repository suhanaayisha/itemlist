import { useState, useEffect } from 'react';
import Items from '../components/Items/Items';
import { useItems } from '../contexts/ItemProvider';
import { Empty } from 'antd';
import HashtagFilter from '../components/HashtagFilter/HashtagFilter';
import {Item} from '../backend/schema';
import { Document } from '../backend/model';

const Favs = () => {
    const allItems = useItems()

    const getTags = allItems.favItems.map(item => {
        return(item.doc.hashtags)
    })

    const tags = [].concat.apply([], getTags);

    const [filteredItems, setFilteredItems] = useState<Document<Item>[]>(allItems.items)

    const filter = (hashtag:string) => {
        let items = allItems.items.filter(item => item.doc.hashtags.includes(hashtag))
        setFilteredItems(items)
      }
    
      useEffect(()=> {
        setFilteredItems(allItems.favItems)
    
      }, [allItems.favItems])
 
    return(
        allItems.favItems.length?
        <div className="page-container container">
            <div className={"mobile-view"} >
                <HashtagFilter tags={tags} filter={(hashtag)=> filter(hashtag)} goBack={() => setFilteredItems(allItems.favItems)}/>
            </div>
            <Items items={filteredItems}/>
        </div>
       :
       <Empty description="You have no favourites"/>
    )
}

export default Favs
// pages/index.js
import { useState, useEffect } from 'react';
import Head from 'next/head';
import Items from '../components/Items/Items';
import HashtagFilter from '../components/HashtagFilter/HashtagFilter';
import { useItems } from '../contexts/ItemProvider';
import {Item} from '../backend/schema';
import { Document } from '../backend/model';

const Home = () => {
  const allItems = useItems()

  const getTags = allItems.items.map(item => {
    return(item.doc.hashtags)
  })

  const tags = [].concat.apply([], getTags);

  const [filteredItems, setFilteredItems] = useState<Document<Item>[]>(allItems.items)

  const filter = (hashtag:string) => {
    let items = allItems.items.filter(item => item.doc.hashtags.includes(hashtag))
    setFilteredItems(items)
  }

  useEffect(()=> {
    setFilteredItems(allItems.items)

  }, [allItems.items])

  return (
    <div className="page-container container">
      <Head>
        <title>Items</title>
      </Head>
      <div className={"mobile-view"} >
        <HashtagFilter tags={tags} filter={(hashtag)=> filter(hashtag)} goBack={() => setFilteredItems(allItems.items)}/>
      </div>
      <Items items={filteredItems}/>
    </div>
  )
}
export default Home;
import '../styles/globals.scss'
import '../styles/index.scss'
import '../components/Items/items.scss'
import '../components/HashtagFilter/HashtagFilter.scss'
import 'antd/dist/antd.css';
import HeaderBar from '../components/HeaderBar/HeaderBar';
import '../components/HeaderBar/HeaderBar.scss';
import { ItemContextProvider } from '../contexts/ItemProvider';

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <div>
        <HeaderBar/>
      </div>
      <div className="content">
        <ItemContextProvider>
          <Component {...pageProps} />
        </ItemContextProvider>
      </div> 
    </div>
  )
}

export default MyApp

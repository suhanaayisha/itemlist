import { createContext, useState, useEffect, useContext} from 'react';
import {Document} from '../backend/model';
import {getAllItems, getFavItems, getFilteredItems} from '../backend/queries'
import {Item} from '../backend/schema'

// I have used contexts just to display my knowledge on the subject, it could have been done without this too
export const useItems = () => {
    return useContext(ItemContext);
  };

const ItemContext =  createContext<{
    items:  Document<Item>[],
    favItems:  Document<Item>[]
  }>({ items: null, favItems: null });

export const ItemContextProvider = (props) => {
    const [items, setItems] = useState([]);
    const [favourites, setFavourites] = useState([]);

    useEffect(() => {
        getAllItems((items:  Document<Item>[]) => setItems(items))
        // getFilteredItems(filter, (items:  Document<Item>[]) => setItems(items))
        getFavItems((items:  Document<Item>[]) => setFavourites(items))
    }, []);

    return (
        <ItemContext.Provider value={{ items: items, favItems: favourites}}>
            {props.children}
        </ItemContext.Provider>
      );
}